package ictgradschool.industry.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TestingOutput {
    public static void main(String[] args) {
        TestingOutput x = new TestingOutput();
        x.start();
    }

    public void start() {
        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};

        List<String> arrayList = new ArrayList<>();

       // Collections.addAll(arrayList, array);


        //doing with a for loop - this one makes sense to me

//        for (int i = 0; i < arrayList.size(); i++) {
//            String lc = arrayList.get(i).toLowerCase();
//            arrayList.set(i, lc);
//        }

        for (String s : array) {
            arrayList.add(s.toLowerCase());
        }

        //doing with foreach loop: this seems worse????
//        int i = 0;
//        for(String element : arrayList){
//            String lc = element.toLowerCase();
//            arrayList.set(i, lc);
//            i++;
//        }

        //doing with iterator - a puzzle
//        int i = 0;
//        Iterator<String> myIter = arrayList.iterator();
//        while (myIter.hasNext()) {
//            String lc = myIter.next().toLowerCase();
//            arrayList.set(i, lc);
//            i++;
//
//        }


        System.out.println(arrayList);
    }
}
